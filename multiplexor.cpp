#include "multiplexor.h"

multiplexor::multiplexor() {}
multiplexor::multiplexor(int _pin0, int _pin1, int _pin2, int _pin3)
{
    this->_pines[0] = _pin0;
    this->_pines[1] = _pin1;
    this->_pines[2] = _pin2;
    this->_pines[3] = _pin3;
}
void  multiplexor::setPuerta(int _puerta)
{
    int r0 = _puerta & 1;
    int r1 = _puerta & 2;
    int r2 = _puerta & 4;
    int r3 = _puerta & 8;

    digitalWrite(this->_pines[3], (r0 == 0 ? LOW : HIGH));
    digitalWrite(this->_pines[2], (r1 == 0 ? LOW : HIGH));
    digitalWrite(this->_pines[1], (r2 == 0 ? LOW : HIGH));
    digitalWrite(this->_pines[0], (r3 == 0 ? LOW : HIGH));
}

void multiplexor::setPines(int _pin0, int _pin1, int _pin2, int _pin3)
{
    this->_pines[0] = _pin0;
    this->_pines[1] = _pin1;
    this->_pines[2] = _pin2;
    this->_pines[3] = _pin3;
}
