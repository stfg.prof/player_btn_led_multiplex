#include "multiplexor.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

SoftwareSerial mySoftwareSerial(12, 13); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);


/**
  DFPLAYER MULTIPLEXORES BTN LED
  Created:  2022/10/22
  Version: 1.
  Authors:   Santiago Fernandez
  Contact:  stfg.prof@gmail.com

  Description: audioplayer a partir del shild dfplayer, que utilizando dos multiplexores
  opera disparando una cacion por btn

  to do:
    cambiar limite 
    ajutar la micro sd (quizas +1 en el puntero dentro del play)
 **/



int puntero = 0;
int limite = 9;
multiplexor muxBtn, muxLed;
void setup() {
  Serial.begin(9600);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, INPUT_PULLUP);

  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);

  pinMode(11, OUTPUT);//este podria ser el enable o el pin de luz ,quizas convenga el enable

  pinMode(A0, INPUT);//el pin de si algo esta sonando o no

  mySoftwareSerial.begin(9600);
  Serial.begin(115200);


  muxBtn.setPines(2, 3, 4, 5);
  muxLed.setPines(7, 8, 9, 10);

  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while (true) {
      delay(0); // Code to compatible with ESP8266 watch dog.
    }
  }
  Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(10);  //Set volume value. From 0 to 30

}

void loop() {

  muxBtn.setPuerta(puntero);

  bool in = digitalRead(6);
  bool isBussy  = digitalRead(A0);

  if (!in) {
    myDFPlayer.play(puntero);
    muxLed.setPuerta(puntero);
    Serial.print("el boton apretado es : ");
    Serial.println(puntero);
  }
  //prender el led si se esta escuchando algo
  if (isBussy ) {
    digitalWrite(11, HIGH);
  } else {
    digitalWrite(11, LOW);
  }
  puntero++;
  if (puntero > limite)puntero = 0;
}
