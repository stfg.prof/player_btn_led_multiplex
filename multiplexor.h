#include <Arduino.h>


class multiplexor
{
  private:
    int _pines[4];

  public:
    multiplexor();
    multiplexor(int _pin0, int _pin1, int _pin2, int _pin3);
    void setPuerta(int _puerta);
    void setPines(int _pin0, int _pin1, int _pin2, int _pin3);
};
